package com.example.demo.domain;

import jakarta.persistence.*;

@Entity
@Table(name = "dishes_categories")
public class DishCategory {

    @Id
    @Column(name = "dishes_categories_id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long Id;

    @Column(name = "dishes_categories_name")
    private String dishesCategoriesName;

    public DishCategory() { }

    public DishCategory(Long Id, String dishesCategoriesName) {
        this.Id = Id;
        this.dishesCategoriesName = dishesCategoriesName;
    }

    public Long getId() { return Id; }
    public void setId(Long Id) { this.Id = Id; }

    public String getDishesCategoriesName() { return dishesCategoriesName; }
    public void setDishesCategoriesName(String dishesCategoriesName) {
        this.dishesCategoriesName = dishesCategoriesName; }
}
