
    create table dishes (
        dishes_id bigint not null,
        carbohydrates varchar(255),
        complexity varchar(255),
        description varchar(255),
        dishes_image varchar(255),
        dishes_name varchar(255),
        duration varchar(255),
        energy_value varchar(255),
        fats varchar(255),
        proteins varchar(255),
        id bigint not null,
        primary key (id))

    create table dishes_categories (
        dishes_categories_id bigint not null,
        dishes_categories_name varchar(255),
        id bigint,
        primary key (dishes_categories_id))

    create table ingredients (
        ingredients_id bigint not null,
        ingredient_measure varchar(255),
        ingredient_name varchar(255),
        id bigint not null,
        primary key (ingredients_id))

    create table meals (
        meals_id bigint not null,
        meals_name varchar(255),
        primary key (meals_id))

    create table orders (
        id bigint not null,
        dishes_date timestamp(6),
        order_date timestamp(6),
        primary key (id))

    create table recipes (
        id bigint not null,
        ingredient_quantity bigint,
        primary key (id))

    create table users (
        users_id bigint not null,
        creation_date timestamp(6),
        date_of_birth timestamp(6),
        email varchar(255),
        sex varchar(255),
        password varchar(255),
        first_name varchar(255),
        id bigint,
        primary key (users_id))

    alter table if exists dishes add constraint UK_cqdt2iqsxvgh89fdvitgfi6ru unique (id)
    alter table if exists ingredients add constraint UK_9v32kv80js4dx4dettw1j390o unique (id)
    alter table if exists users add constraint UK_6dotkott2kjsp8vw4d0m25fb7 unique (email)
    alter table if exists dishes add constraint FK8eqp81lmwp90s7isoalsh008y foreign key (dishes_id) references recipes
    alter table if exists dishes add constraint FKq7o8uoa2pkdpf663x2r37h0gr foreign key (id) references dishes
    alter table if exists dishes_categories add constraint FKca5oxhndhxlvbqnjvod7eh5jg foreign key (id) references dishes_categories
    alter table if exists dishes_categories add constraint FKf08kn7s8jwg49u0kvlqdbditq foreign key (dishes_categories_id) references dishes
    alter table if exists ingredients add constraint FKn79yyxdelajrvlwoex6yfouem foreign key (id) references ingredients
    alter table if exists ingredients add constraint FKf9be56qre9rmb350ptsxpm6aj foreign key (ingredients_id) references recipes
    alter table if exists meals add constraint FK73ntar2427xl3i63osees9qxx foreign key (meals_id) references orders
    alter table if exists users add constraint FKhwxxsovt1xxol6upd7iq26m8w foreign key (id) references users
    alter table if exists users add constraint FKrxstraob258ofubtc9pf4rlds foreign key (users_id) references orders
